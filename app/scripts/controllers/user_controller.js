'use strict';

angular.module('myApp').controller('UserController', ['$scope', 'UserService', function($scope, UserService) {
    var vmAS = this;
        
        //this is to understanding whow json object will form 
        vmAS.person = {
            id : 0,
            name:'',
            country:''
        };

        vmAS.users = [];

    vmAS.fetchUsers = function () {

        UserService.fetchAllUsers()
            .then(
            function(d) {
                vmAS.users = d;
                console.log(JSON.stringify(vmAS.users));
            },
            function(errResponse){
                console.error('Error while fetching Users'+errResponse);
            }
        );
    };

    vmAS.fetchUsers();

    vmAS.add = function () {
        UserService.createUser(vmAS.person)
            .then(function(d) {
              // your code goes here
              vmAS.users = d;
                console.log(JSON.stringify(vmAS.users));
            },
            function(errResponse){
                console.error('Error while creating User'+errResponse);
            }
        );
    };

     vmAS.edit = function (id) {
        UserService.updateUser(id)
            .then(function(d) {
              // your code goes here
              vmAS.person = d;
                console.log(JSON.stringify(vmAS.person));
            },function(errResponse){
                console.error('Error while updating User'+errResponse);
            }
        );
     };

}]);
